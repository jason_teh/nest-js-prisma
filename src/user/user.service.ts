import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import * as argon from 'argon2';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}
  create(createUserDto: CreateUserDto) {
    return 'This action adds a new user';
  }

  async findAll() {
    const user = await this.prisma.user.findMany({
      take: 10,
      select: {
        id: true,
        uuid: true,
        email: true,
        first_name: true,
        last_name: true,
      },
    });

    return user;
  }

  async findOne(uuid: string) {
    const user = await this.prisma.user.findUniqueOrThrow({
      where: { uuid },
      select: {
        id: true,
        uuid: true,
        email: true,
        first_name: true,
        last_name: true,
      },
    });

    if (!user) throw new NotFoundException('User not found');

    return user;
  }

  update(uuid: string, updateUserDto: UpdateUserDto) {
    return `This action updates a #${uuid} user`;
  }

  remove(uuid: string) {
    return `This action removes a #${uuid} user`;
  }

  async passwordHash(password) {
    return await argon.hash(password);
  }
}
