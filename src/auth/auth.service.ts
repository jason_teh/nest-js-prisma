import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { PrismaService } from '../prisma/prisma.service';
import { UserService } from '../user/user.service';
import { LoginDto, SignUpDto } from './dto/auth.dto';
import * as argon from 'argon2';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async login(data: LoginDto) {
    const user = await this.prisma.user.findUnique({
      where: {
        email: data.email,
      },
      select: {
        id: true,
        uuid: true,
        email: true,
        first_name: true,
        last_name: true,
        password: true,
      },
    });

    if (!user) throw new UnauthorizedException('Invalid credentials');

    const authenticated = await argon.verify(user.password, data.password);

    if (!authenticated) throw new UnauthorizedException('Invalid credentials');

    delete user.password;

    const payload = { email: user.email, sub: user.uuid };

    user['access_token'] = `Bearer ${this.jwtService.sign(payload)}`;
    return user;
  }

  async signUp(data: SignUpDto) {
    const hash = await this.userService.passwordHash(data.password);
    try {
      const user = await this.prisma.user.create({
        data: {
          ...data,
          password: hash,
        },
        select: {
          id: true,
          uuid: true,
          email: true,
          first_name: true,
          last_name: true,
        },
      });

      return user;
    } catch (error) {
      throw new BadRequestException(
        'Something wrong please check the credentials that was entered',
      );
    }
  }
}
